## Flatbuffers proxy repository for hell

This is proxy repository for [Flatbuffers library](http://google.github.io/flatbuffers/), which allows you to build and install it using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have a problem with the installation of Flatbuffers using hell, have an improvement idea, or want to request support for other versions of Flatbuffers, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in Flatbuffers itself, please create issue on [Flatbuffers issue tracker](https://github.com/google/flatbuffers/issues), because here we don't do any kind of Flatbuffers development.
